<?php

namespace Database\Seeders;

use Carbon\Traits\Timestamp;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->insert([
            'project_name' => 'Projecto de prueba',
            'description' => 'Descripción del projecto de prueba',
        ]);

        DB::table('projects')->insert([
            'project_name' => 'Projecto de prueba 2',
            'description' => 'PruebaPruebaPruebaPruebaPruebaPruebaPrueba',
        ]);
        DB::table('projects')->insert([
            'project_name' => 'Projecto de prueba 3',
            'description' => 'Prueba',
        ]);
    }
}
