<?php

namespace Database\Seeders;

use App\Models\User;

use Illuminate\Database\Seeder;

use Illuminate\Support\Carbon;

use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Salvador',
            'surname' => 'Pablo',
            'role_id' => 3,
            'email' => 'salvadorpablol092@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
        ]);

        User::create([
            'name' => 'Test1',
            'surname' => '-',
            'role_id' => 2,
            'email' => 'test1@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
        ]);

        User::create([
            'name' => 'Test2',
            'surname' => '-',
            'role_id' => 1,
            'email' => 'test2@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
        ]);

        User::create([
            'name' => 'Test3',
            'surname' => '-',
            'role_id' => 1,
            'email' => 'test3@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
        ]);
    }
}
