@extends('layouts.app')

@section('content')

<body>
    <div class="container-fluid border h-100 w-100">
        <div class="row">
            <h1 class="mt-5 ml-5">Manage user roles</h1>
        </div>
        <div class="row border w-100 mt-3 h-100">
            <div class="col-12 mt-3">
                <form action="/users/{{id}}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">

                    <h5>Select a user</h5>
                    <select multiple class="form-control" name="id" id="id">
                        <label for="id"></label>
                        @foreach($users as $user)
                        <option value="{{$user->id}}" name="id">{{$user->name}}</option>
                        @endforeach
                    </select>

                    <label for="role_id" class="mt-5">Select a new role for the user:</label>
                    <select id="role_id" class="form-control">
                        @foreach($roles as $role)
                        <option selected>{{$role->name}}</option>
                        @endforeach
                    </select>
                    <input class="btn btn-primary mt-5" type="submit" value="Assign new role">

                </form>

            </div>

        </div>
    </div>
</body>

</html>
@endsection