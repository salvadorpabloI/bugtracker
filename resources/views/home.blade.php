@extends('layouts.app')

@section('content')

<body>
    <div class="container-fluid border h-100 w-100">
    <div class="row w-100">
            <div class="row w-50">
                <h1 class="mt-5 ml-5">Tickets by priority</h1>
            </div>
            
            <div class="row w-50">
                <h1 class="mt-5 ml-5">Tickets by status</h1>
            </div>
        </div>

        <div class="row w-100">
            <div class="row w-50">
                <h1 class="mt-5 ml-5">Tickets by project</h1>
            </div>
            
            <div class="row w-50">
                <h1 class="mt-5 ml-5">Tickets by user</h1>
            </div>
        </div>
    </div>

</body>
@endsection