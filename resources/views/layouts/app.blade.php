<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'BugTracker') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        @import url('https://fonts.googleapis.com/css2?family=Playfair+Display:wght@800&display=swap');

        body {
            height: 100vh;
            font-size: 18px;
        }

        .logo {
            width: 100px;
            height: 100px;
            margin: 10px auto 30px;
        }

        .headerLogo {
            background-color: #3B945E;
        }

        .textoSidebar {
            font-family: 'Playfair Display', serif;
            color: black !important;
            font-size: 20px;
        }

        .nav.navbar-light li a {
            color: #182628 !important;
        }

        /*tabla responsive*/
        table {
            display: block;
            overflow-x: auto;
            border-spacing: 10px;
        }

        /*Profile blade*/

    </style>
</head>

<body>
    <div class="main h-100 d-flex flex-column">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ route('home') }}">
                    <img src="../img/nombreLogo.png" alt="BugTracker">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto ml-2">
                        <!-- @auth
                        <li>Logged as: {{Auth::user()->name}}</li>
                        @endauth -->
                        @auth
                        <li class="ml-5"><a class="btn btn-primary btn-md" href="{{ url('/tickets/create') }}">Crear ticket</a></li>
                        @endauth
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @endif

                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @auth
        <div class="container-fluid d-flex flex-fill">
            <div class="row flex-fill">
                <div class="col-sm-3 col-md-3 col-lg-2 bg-white shadow-lg pr-3 menuContainer d-flex flex-column sm-2 md">
                    <div class="logo">
                        <img src="../img/logoBugtracker.png" alt="Logo BugTracker" style="border-radius: 50%;">
                    </div>
                    <hr>
                    <nav class="nav navbar-light bg-white p-3 h-100 justify-content-center">
                        <ul class="navbar-nav flex-column textoSidebar">
                            <li class="nav-item p-2"><a href="{{ route('home') }}" class="nav-link">Home</a></li>
                            <li class="nav-item p-2"><a href="{{ route('myProjects') }}" class="nav-link">My projects</a></li>
                            <li class="nav-item p-2"><a href="{{ route('myTickets') }}" class="nav-link">My tickets</a></li>
                            <li class="nav-item p-2"><a href="{{ url('/users') }}" class="nav-link">Manage users roles</a></li>
                            <li class="nav-item p-2"><a href="{{ route('manageProjects') }}" class="nav-link">Manage projects</a></li>
                            <li class="nav-item p-2"><a href="{{ url('/users/show') }}" class="nav-link">User profile</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col p-0">
                    @yield('content')
                </div>
            </div>
        </div>
        @endauth

        @guest
        <div class="col p-0">
            @yield('content')
        </div>
        @endguest

    </div>
</body>

</html>