@extends('layouts.app')

@section('content')

<body>
    <div class="container-fluid border h-100 w-100">
        <div class="row">
            <h1 class="mt-5 ml-5">Hello, {{$user->name}}</h1>
        </div>
        <div class="row col-12 border w-100 mt-3">

            @if (\Session::has('Success'))
            <div class="alert alert-success">
                <ul>
                    <li>{!! \Session::get('Success') !!}</li>
                </ul>
            </div>
            @endif
            <div class="col-12 mt-3">
                <div class="card d-flex">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Profile</h4>
                        <p class="card-category">See or edit your profile</p>
                    </div>
                    <div class="card-body">
                        <form action="/users/{{$user->id}}" method="post">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group bmd-form-group">
                                        <label for="name">Name</label>
                                        <input name="name" type="text" class="form-control" value="{{ old('name') ?? $user->name}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group bmd-form-group">
                                        <label for="surname">Surname</label>
                                        <input name="surname" type="text" class="form-control" value="{{old('surname') ? old('surname') : $user->surname}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group bmd-form-group">
                                        <label for="email">Email</label>
                                        <input name="email" type="text" class="form-control" value="{{old('email') ? old('email') : $user->email}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-4">
                                    <div class="form-group bmd-form-group">
                                        <label for="role_id" class="mt-5">Role</label>
                                        <select name="role_id" class="custom-select">
                                            <option selected value="{{old('role_id') ? old('role_id') : $user->role_id}}">{{ $user->role->name }}</option>
                                            @foreach($roles as $role)
                                            @if ($role->name != $user->role->name)
                                            <option value="{{old('id') ? old('id') : $role->id}}" name="role_id">{{$role->name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            @foreach ($errors->all() as $error)
                            <div class="alert alert-danger">
                                <li>{{$error}}</li>
                            </div>
                            @endforeach

                            <input class="btn btn-primary mt-5" type="submit" value="Update">

                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>
</body>

</html>
@endsection