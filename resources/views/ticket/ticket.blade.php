@extends('layouts.app')

@section('content')

<body>
    <div class="container-fluid border h-100 w-100">
        <div class="row">
            <h1 class="mt-5 ml-5">Create ticket</h1>
        </div>
        <div class="row col-12 mt-3 pt-5 border">
            <form class="col-12" action="/tickets" method="post">
                @csrf

                <div class="row">

                    <div class="col-6 pl-5">
                        <label for="title" class="form-label">Title:</label>
                        <input type="text" name="title" value="{{ old('title')}}" class="form-control">
                    </div>

                    <div class="col-6">
                        <label for="description" class="form-label">Description:</label>
                        <input type="text" name="description" class="form-control" value="{{ old('description') }}">
                    </div>

                </div>

                <div class="row mt-5">

                    <div class="col-6 pl-5 mt-4">
                        <h5>Select ticket's priority</h5>
                        <select label="priority" name="priority" id="priority" class="form-select"></label>
                            <option value="Low">Low</option>
                            <option value="Medium">Medium</option>
                            <option value="High">High</option>
                        </select>
                    </div>

                    <div class="col-6 mt-4">
                        <h5>Select ticket's status</h5>
                        <select label="status" name="status" id="status" class="form-select"></label>
                            <option value="Open">Open</option>
                            <option value="Closed">Closed</option>
                        </select>
                    </div>

                </div>
                
                <div class="row mt-5">

                    <div class="col-6 pl-5 mt-4">
                        <h5>Who is this ticket for?</h5>
                        <label for="user_id"></label>
                        <select name="user_id" id="user_id">
                            @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </select>

                    </div>

                    <div class="col-6 mt-4">
                        <h5>Wich project this ticket belongs to?</h5>
                        <label for="project_id"></label>
                        <select name="project_id" id="project_id">
                            @foreach($projects as $project)
                            <option value="{{$project->id}}">{{$project->project_name}}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="row mt-5 d-flex justify-content-center">
                    <div class="mt-4">
                        <h5>Select type of ticket</h5>
                        <select label="type" name="type" id="type" class="form-select"></label>
                            <option value="Bug">Bug</option>
                            <option value="Error">Error</option>
                            <option value="Fix">Fix</option>
                        </select>
                    </div>
                </div>

                <div class="row d-flex justify-content-center mt-5">
                    <input class="btn btn-primary btn-md" type="submit" value="Create ticket">
                </div>

            </form>
        </div>
    </div>


</body>
@endsection