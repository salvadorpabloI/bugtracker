@extends('layouts.app')

@section('content')

<body>
    <div class="container-fluid border h-100 w-100">
        <div class="row">
            <h1 class="mt-5 ml-5">Details for project {{$project->id}}</h1>
        </div>
        <div class="row">
            <ul class="nav ml-5">
                <li class="nav-item pr-5"><a link href="{{ route('myProjects') }}">Back to list</a></li>
            </ul>
        </div>
        
        <div class="row w-100 mt-3 pt-5">

            <div class="w-50 pl-5">
                <h5>Project name:</h5>
                <p class="text-uppercase pt-3">{{$project->project_name}}</p>
            </div>

            <div class="w-50 pl-5">
                <h5>Project description:</h5>
                <p class="text-uppercase pt-3">{{$project->description}}</p>
            </div>

        </div>

        <hr>

        <div class="row w-100">

            <div class="col-5">
                <h4>Assigned Personnel</h4>
                <p>Current users on this project:</p>
                <table class="table table-striped table-responsive">
                    <tr>
                        <th>User name</th>
                        <th>Email</th>
                        <th>Role</th>
                    </tr>
                    @forelse($project->users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->role->name}}</td>
                        </tr>
                    @empty
                    <tr>
                        <td colspan="3">There's no users in this project</td>
                    </tr>
                    @endforelse
                </table>
            </div>
            <div class="col-7">
            <h4>Tickets for this project</h4>
                <p>Condensed ticket details</p>
                <h5 class="mb-3 mt-3">Ticket list:</h5>
                <table class="table table-striped table-responsive">
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Priority</th>
                        <th>Status</th>
                        <th>User</th>
                        <th>Project</th>
                        <th>Type</th>
                    </tr>
                    @forelse($tickets as $ticket)
                    @if($user->id == $ticket->user_id)
                    <tr>
                        <td>{{$ticket->title}}</td>
                        <td>{{$ticket->description}}</td>
                        <td>{{$ticket->priority}}</td>
                        <td>{{$ticket->status}}</td>
                        <td>{{$ticket->user_id}}</td>
                        <td>{{$ticket->project_id}}</td>
                        <td>{{$ticket->type}}</td>
                    </tr>
                    @endif
                    @empty
                    <tr>
                        <td colspan="3">You have no tickets</td>
                    </tr>
                    @endforelse
                </table>
            </div>
        </div>
    </div>

</body>
@endsection